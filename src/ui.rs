use iced::{Align, Application, button, Button, Column, Element, Row, Sandbox, Settings, Text, text_input, TextInput, Length, HorizontalAlignment, Container, ProgressBar, Radio};
use log::warn;
use std::collections::HashMap;
use rayon::ThreadBuilder;
use crate::{CompressionSettings, CompressionType};
use std::ops::RangeInclusive;
use std::alloc::Layout;

/// Warning text if invalid configuration is detected
fn warnings(pos: usize) -> String {
    // why can't this be static? oh man...
    let _warnings = ["The paths are the same.\n".to_string(),
        "Recursive paths.\n".to_string(),
        "Source path is not a directory.\n".to_string(),
        "Destination path is not a directory.\n".to_string(),
        "No Compression Level is set".to_string()];

    _warnings[pos].clone()
}


#[derive(Clone, Eq, Hash, PartialEq)]
enum DispatchDisabledReason {
    SourcePath,
    DestinationPath,
    SamePath,
    SubPath,
    Level,
}

/// Application State
pub struct BackupGui {
    settings: CompressionSettings,
    // this is sadly necessary to accommodate an empty TextInput
    level_string: String,
    // in case BackupGui should be cloneable, this should be made into an Arc<Mutex<...>>
    // if unnecessary I would rather avoid such overhead
    dispatch_disable: HashMap<DispatchDisabledReason, bool>,
    dispatched: bool,

    src_dir_box: text_input::State,
    dst_dir_box: text_input::State,
    level_box: text_input::State,
    dispatch_warning: String,
    dispatch_button: button::State,

    // dispatched info
    progress_text: String,
    cancel_button: button::State,
}

impl Default for BackupGui {
    fn default() -> Self {
        BackupGui {
            settings: Default::default(),
            level_string: "3".to_owned(),
            dispatch_disable: HashMap::new(),
            dispatched: false,
            dst_dir_box: Default::default(),
            src_dir_box: Default::default(),
            level_box: Default::default(),
            dispatch_warning: "Good to go".to_owned(),
            dispatch_button: Default::default(),

            progress_text: Default::default(),
            cancel_button: Default::default(),
        }
    }
}

#[derive(Clone, Debug)]
pub enum Message {
    SrcDirChanged(String),
    DstDirChanged(String),
    LevelChanged(String),
    TypeChanged(CompressionType),
    ModeChanged(bool),
    Dispatch,
}

impl Sandbox for BackupGui {
    type Message = crate::ui::Message;

    /// default values for the Backup dialogue
    fn new() -> Self {
        BackupGui::default()
    }


    fn title(&self) -> String {
        "Backup Configuration".to_owned()
    }

    /// handles GUI events in form of messages
    fn update(&mut self, message: Self::Message) {
        match message {
            Message::SrcDirChanged(path) => {
                self.settings.src_dir = path;
                self.verify_path();
            }
            Message::DstDirChanged(path) => {
                self.settings.dst_dir = path;
                self.verify_path();
            }
            Message::LevelChanged(level) => {
                if level.is_empty() {
                    self.settings.level = 0;
                    self.level_string = level;
                    self.disable_dispatch(DispatchDisabledReason::Level);
                    return;
                }
                let lev: u32 = match level.parse() {
                    Ok(val) => val,
                    Err(err) => {
                        warn!("Parsing error occured at LevelChanged Message: {}", err.to_string());
                        self.settings.level
                    } // a parse error indicates a non numeric input, which can happily ignored
                };
                self.assign_sanitized_level(lev);
            }
            Message::Dispatch => {
                self.dispatch();
            }
            Message::TypeChanged(val) => {}
            Message::ModeChanged(val) => {}
        }
    }

    fn view(&mut self) -> Element<Message> {
        match self.dispatched {
            false => {
                let src_label = iced::Text::new("Source: ");
                let dst_label = iced::Text::new("Archive: ");
                let level_label = iced::Text::new("Compression level: ");
                let type_label = iced::Text::new("Algorithm: ");

                

                let mode_radio = Radio::new(false,
                                                "Dictionary Mode",
                                                Some(false),
                                                |val| Message::ModeChanged(val));

                let src_dir = TextInput::new(&mut self.src_dir_box,
                                             "Enter directory to be watched here",
                                             self.settings.src_dir.as_str(),
                                             |path| Message::SrcDirChanged(path))
                    .padding(5);
                let dst_dir = TextInput::new(&mut self.dst_dir_box,
                                             "Enter directory to contain archive",
                                             self.settings.dst_dir.as_str(),
                                             |path| Message::DstDirChanged(path))
                    .padding(5);
                let level_box = TextInput::new(&mut self.level_box,
                                               "Enter compression level",
                                               self.level_string.as_str(),
                                               |level| Message::LevelChanged(level))
                    .padding(5);
                let dispatch_warning = Text::new(self.dispatch_warning.as_str());

                let dispatch_button = Container::new(match self.dispatch_disable.is_empty() {
                    true => Button::new(&mut self.dispatch_button,
                                        Text::new("Dispatch archiver"))
                        .on_press(Message::Dispatch),
                    false => Button::new(&mut self.dispatch_button,
                                         Text::new("Can't Dispatch"))
                }.width(Length::Fill))
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .align_y(Align::End);


                // this builds the layout in form of a widget tree
                iced::Element::from(
                    Column::new()
                        .push(Row::new().push(src_label).push(src_dir).spacing(5).align_items(Align::Center))
                        .push(Row::new().push(dst_label).push(dst_dir).spacing(5).align_items(Align::Center))
                        .push(Row::new().push(level_label).push(level_box).spacing(5).align_items(Align::Center))
                        .push(match self.settings.comp_type {
                            CompressionType::ZSTD => Container::new(mode_radio),
                            CompressionType::LZ4 => Container::new(mode_radio)
                                .width(Length::Units(0))
                                .height(Length::Units(0))
                        })
                        .push(dispatch_warning)
                        .push(dispatch_button).spacing(5).padding(5))
            }
            true => {
                let dispatched_text = Text::new("Daemon launched!")
                    .width(Length::Fill)
                    .horizontal_alignment(HorizontalAlignment::Center);

                let progress_text = Text::new(self.progress_text.as_str())
                    .horizontal_alignment(HorizontalAlignment::Center);

                let progress_bar = Container::new(ProgressBar::new(
                    RangeInclusive::new(0.0, 1.0),
                    0.0)).height(Length::Fill).align_y(Align::End);

                 Element::from(Column::new()
                    .push(dispatched_text)
                    .push(progress_text)
                    .push(progress_bar).spacing(5)
                    )
            }
        }
    }
}


impl BackupGui {
    /// this verifies correct and sensible paths have been provided and disables dispatch if not
    fn verify_path(&mut self) {
        match self.settings.src_dir.eq(&self.settings.dst_dir) {
            true => self.disable_dispatch(DispatchDisabledReason::SamePath),
            false => self.enable_dispatch(DispatchDisabledReason::SamePath),
        }

        match std::fs::read_dir(&self.settings.src_dir).is_ok() | self.settings.src_dir.is_empty() {
            true => self.enable_dispatch(DispatchDisabledReason::SourcePath),
            false => self.disable_dispatch(DispatchDisabledReason::SourcePath),
        }

        match std::fs::read_dir(&self.settings.dst_dir).is_ok() | self.settings.src_dir.is_empty() {
            true => self.enable_dispatch(DispatchDisabledReason::DestinationPath),
            false => self.disable_dispatch(DispatchDisabledReason::DestinationPath),
        }

        // to prevent recursion, verify that dst_dir is not a subpath
        match self.settings.dst_dir.contains(&self.settings.src_dir) |
            self.settings.src_dir.contains(&self.settings.dst_dir) {
            true => self.disable_dispatch(DispatchDisabledReason::SubPath),
            false => self.enable_dispatch(DispatchDisabledReason::SubPath),
        }
    }

    fn assign_sanitized_level(&mut self, level: u32) {
        match self.settings.comp_type {
            CompressionType::ZSTD => {
                if level < 1 {
                    self.settings.level = 1
                } else if level > 19
                {
                    self.settings.level = 19
                } else {
                    self.settings.level = level
                }
            }
            CompressionType::LZ4 => {
                self.settings.level = 1
            }
        }
        self.level_string = self.settings.level.to_string();

        self.enable_dispatch(DispatchDisabledReason::Level)
    }

    /// adds reason to dispatch_disable HashMap and disables the dispatch button
    fn disable_dispatch(&mut self, reason: DispatchDisabledReason) {
        if self.dispatch_warning.contains("Good to go") {
            self.dispatch_warning = self.dispatch_warning.replacen("Good to go", "", 1);
        }
        if !self.dispatch_disable.contains_key(&reason) {
            self.dispatch_disable.insert(reason.clone(), true);

            let msg = match reason {
                DispatchDisabledReason::SamePath => warnings(0),
                DispatchDisabledReason::SubPath => warnings(1),
                DispatchDisabledReason::SourcePath => warnings(2),
                DispatchDisabledReason::DestinationPath => warnings(3),
                DispatchDisabledReason::Level => warnings(4)
            };
            self.dispatch_warning.push_str(msg.as_str());
        }
    }

    /// removes the specified reason and enables dispatch if all reasons have been removed
    fn enable_dispatch(&mut self, reason: DispatchDisabledReason) {
        if self.dispatch_disable.contains_key(&reason) {
            self.dispatch_disable.remove(&reason);

            match reason {
                DispatchDisabledReason::SamePath => {
                    self.dispatch_warning = self.dispatch_warning.replacen(&warnings(0), "", 1);
                }
                DispatchDisabledReason::SubPath => {
                    self.dispatch_warning = self.dispatch_warning.replacen(&warnings(1), "", 1);
                }
                DispatchDisabledReason::SourcePath => {
                    self.dispatch_warning = self.dispatch_warning.replacen(&warnings(2), "", 1);
                }
                DispatchDisabledReason::DestinationPath => {
                    self.dispatch_warning = self.dispatch_warning.replacen(&warnings(3), "", 1);
                }
                DispatchDisabledReason::Level => {
                    self.dispatch_warning = self.dispatch_warning.replacen(&warnings(4), "", 1);
                }
            }

            if self.dispatch_warning.is_empty() {
                self.dispatch_warning = "Good to go".to_owned();
            }
        }
    }


    /// dispatch launches a new thread, which will execute the actual compression
    fn dispatch(&mut self) {
        //TODO: offer choice to user
        rayon::ThreadPoolBuilder::new().num_threads(16).build_global().unwrap();

        let set = self.settings.clone();

        // even though rayon is used to schedule the compression worker threads it does not make
        // sense to also use it here.
        std::thread::spawn(|| {
            crate::CompressionHandler::new(set).dispatch()
        });
        self.dispatched = true;
    }
}
