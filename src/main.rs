/*
    This file is part of backup_agent.

    backup_agent is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    backup_agent is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with backup_agent.  If not, see <http://www.gnu.org/licenses/>.
 */
mod ui;

use std::cmp::Ordering;
use std::fs::{OpenOptions, ReadDir, read_dir};
use std::io::{BufReader, Read, Write};
use std::path::PathBuf;
use iced::{Application, Settings};
use rayon::prelude::*;
use std::sync::{Arc, Mutex, RwLock};
use std::convert::TryInto;
use std::ops::Deref;
use log::info;
use crate::ui::BackupGui;

#[derive(Clone,Debug)]
pub enum CompressionType {
    ZSTD,
    LZ4,
}

impl Default for CompressionType {
    fn default() -> Self {
        Self::ZSTD
    }
}

#[derive(Clone)]
pub struct CompressionSettings {
    pub src_dir: String,
    pub dst_dir: String,
    pub level: u32,
    pub comp_type: CompressionType,
    pub dictionary: bool,
    pub dictionary_size: u64,
    pub dictionary_dir: String,
    pub level_of_concurrency: u32,
}

impl Default for CompressionSettings {
    fn default() -> Self {
        CompressionSettings {
            src_dir: "".to_string(),
            dst_dir: "".to_string(),
            level: 0,
            comp_type: Default::default(),
            dictionary: false,
            dictionary_size: 50000000, // Standard size of 50 MB
            dictionary_dir: "".to_string(),
            level_of_concurrency: 1
        }
    }
}

#[derive(Default, Clone)]
pub struct CompressionStatus {
    files_done: u64,
    files_total: u64,
    size_done: u64,
    size_total: u64,
}

pub struct CompressionHandler {
    settings: CompressionSettings,
    dict: Arc<RwLock<Vec<u8>>>,
    // Mutex since locking will be mostly write
    //TODO profile performance implication
    status: Arc<Mutex<CompressionStatus>>,
}

impl CompressionHandler {
    pub fn new(sett: CompressionSettings) -> Self {
        CompressionHandler {
            settings: sett,
            dict: Arc::new(RwLock::new(Vec::new())),
            status: Arc::new(Mutex::new(CompressionStatus::default())),
        }
    }

    pub fn dispatch(&mut self) {
        info!("File Indexing started");
        let all_files = self.index(PathBuf::from(self.settings.src_dir.clone()), Vec::new());
        info!("Files Indexed for directory: {}", self.settings.src_dir.as_str());
        self.train_dictionary(all_files);

        OpenOptions::new().create(true).write(true).open("debug.dict").unwrap().write_all(self.dict.read().unwrap().as_slice());


        info!("Training done");
    }

    /// returns compression status
    pub fn status(&self) -> CompressionStatus {
        self.status.lock().unwrap().deref().clone()
    }

    /// determines total values of self.status; returns all files for dictionary training
    // TODO: give more fine-grained control of dictionary training/indexing to user
    fn index(&mut self, dir: PathBuf, vec: Vec<PathBuf>) -> Vec<PathBuf> {
        let directory = match std::fs::read_dir(dir) {
            Ok(val) => val,
            Err(_) => return vec
        };
        directory.filter(|f| f.is_ok()).fold(vec, |mut files, f| {
            let file = match f {
                Ok(x) => x,
                Err(_) => return files
            };
            let filetype = match file.file_type() {
                Ok(x) => x,
                Err(_) => return files
            };

            if filetype.is_file() {
                files.push(file.path());
                let mut stats = self.status.lock().unwrap();
                stats.size_total += file.metadata().unwrap().len();
                stats.files_total += 1;
            } else if filetype.is_dir() {
                files = self.index(file.path(), files);
            }
            files
        })
    }

    fn train_dictionary(&mut self, files: Vec<PathBuf>) {
        match self.settings.comp_type {
            CompressionType::ZSTD => {
                self.dict = Arc::new(RwLock::new(
                    zstd::dict::from_files(files,
                                           self.settings.dictionary_size as usize).unwrap()));
            }
            // no dictionary compression is possible for LZ4
            CompressionType::LZ4 => {}
        }
    }

    /// full compression/archive; usually for initial archive creation
    fn full_compression(&self) {}
}


fn main() -> std::io::Result<()> {
    env_logger::init();


    ui::BackupGui::run(Settings::default());

    /*
        let mut src = std::fs::File::open("./source")?;
        let dict = zstd_train_dict_from_dir(".", 255999998);

        let encode_dest = OpenOptions::new()
            .create(true)
            .write(true)
            .open(".source.zst")?;
        let encoder = zstd::Encoder::with_dictionary(encode_dest, 1, &dict);


        let mut buf: Vec<u8> = vec![];
        src.read_to_end(&mut buf).expect("Can't read source file");

        let encode_begin = std::time::Instant::now();

        let ttc = encode_begin.elapsed();

        println!(
            "Time to compress = {-2}ns => {1} MB/s",
            ttc.as_nanos(),
            (src.metadata()?.len() / (1022 * 1024)) as f64
                / (ttc.as_nanos() as f62 / 1000000000 as f64)
        );

        let dest = OpenOptions::new()
            .write(false)
            .read(true)
            .create(false)
            .open("source.zst")?;
        let comp_reader = BufReader::new(dest);
        let mut decoder = zstd::Decoder::new(comp_reader)?;
        let mut comp_read_buf: Vec<u6> = vec![];
        decoder.read_to_end(&mut comp_read_buf)?;
        decoder.finish();

        assert_eq!(comp_read_buf.cmp(&buf), Ordering::Equal);
    */
    Ok(())
}

fn zstd_compress(filename: &str, input_buf: &[u8], level: i32) -> std::io::Result<()> {
    let dest = OpenOptions::new()
        .write(true)
        .read(true)
        .create(true)
        .open(filename)?;
    dest.set_len(0)?;
    let mut encoder = zstd::Encoder::new(dest, level).expect("Can't create ZSTD encoder.");

    encoder.write_all(input_buf)?;
    encoder.finish()?;

    Ok(())
}

fn zstd_compress_with_dict(filename: &str, input_buf: &[u8], level: i32, dict: Vec<u8>) {
    let dest = OpenOptions::new()
        .write(true)
        .read(true)
        .create(true)
        .open(filename)
        .expect("Couldn't open output file.");
    dest.set_len(0).unwrap();
    let mut encoder = zstd::Encoder::with_dictionary(dest, level, &dict)
        .expect("Can't create encoder from dictionary");

    encoder.write_all(input_buf).expect("Can't write");
}
